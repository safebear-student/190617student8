from selenium import webdriver
import json
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class Parameters():
    def __init__(self):
        self.loadConfig()

        self.w = webdriver.Chrome()
        # self.w = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)

        self.rootURL = "http://automate.safebear.co.uk"
        self.w.implicitly_wait(5)


    def loadConfig(self):

        config = json.load(open('../config.json'))
        self.username = config['username']
        self.password = config['password']
