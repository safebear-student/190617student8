from page_objects import PageObject, PageElement, MultiPageElement


class LoginPage(PageObject):
    home_link = PageElement(link_text="Home")
    username_field = PageElement(id_ = "myid")
    password_field = PageElement(id_ = "mypass")
    all_fields = MultiPageElement(xpath = '//input[@type="text"]')

    def check_page(self):
        return "Sign In" in self.w.title

    def login(self, mainpage, username, password):
        self.username_field.send_keys(username)
        self.password_field.send_keys(password)
        self.password_field.submit()
        return mainpage.check_page()

    def multi_page_elements_test(self, text):
        self.all_fields = text

        return self.username_field.get_attribute("value") == text and self.password_field.get_attribute("value") == text


    def home(self, welcomepage):
        self.home_link.click()
        return welcomepage.check_page()
