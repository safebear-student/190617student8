from page_objects import PageObject, PageElement
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC


class MainFramePage(PageObject):
    logo = PageElement(tag_name="img")

    def check_page(self):
        return self.logo.is_displayed()


    def click_on_logo(self):
        self.logo.click()
