from page_objects import PageObject, PageElement
from page41_frames_page_main_frame import MainFramePage

class FramesPage(PageObject):
    mainframe = PageElement(name="main")

    def check_page(self):
        return "Frame Page" in self.w.title


    def switch_click_on_main_frame(self):
        self.w.switch_to_frame(self.mainframe)

        mainframe = MainFramePage(self.w)
        mainframe.click_on_logo()

        return mainframe.check_page()


