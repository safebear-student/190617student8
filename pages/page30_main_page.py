from page_objects import PageObject, PageElement
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC


class MainPage(PageObject):
    window_handle =0

    logout_link = PageElement(link_text="Logout")
    num1_field = PageElement(id_ = "num1")
    num2_field = PageElement(id_ = "num2")
    submit_add_button = PageElement(xpath="//input[@value='Submit']")
    add_result_field = PageElement(id_ = "addResult")
    saysomething_button = PageElement(xpath = "//button[@onclick='sayFunction()']")
    saySomething_field = PageElement(id_ = "saySomething")
    frame_button = PageElement(link_text="Go to the frames »")


    def check_page(self):
        return "Logged In" in self.w.title

    def logout(self, welcomepage):
        self.logout_link.click()
        return welcomepage.check_page()

    def say_something(self, text):
        self.saysomething_button.click()

        try:
            element = WebDriverWait(self.w, 10).until(EC.alert_is_present())
        except TimeoutError:
            print('Waited 10 seconds and popup didnt appear')

        alert = self.w.switch_to_alert()
        alert.send_keys(text)
        alert.accept()
        return text in self.saySomething_field.text

    def add(self, v1, v2):
        select1 = Select(self.num1_field)
        select2 = Select(self.num2_field)
        select1.select_by_value(str(v1))
        select2.select_by_value(str(v2))

        self.submit_add_button.click()

        return str(int(v1)+int(v2)) in self.add_result_field.text

    def goto_frames(self, frames_page):
        self.window_handle = self.w.current_window_handle
        self.frame_button.click()
        #switch to new window in rather hacky way?
        #for handle in self.w.window_handles:
        self.w.switch_to_window(self.w.window_handles[len(self.w.window_handles)-1])

        return frames_page.check_page()


