import base_test

class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        # step 1: click on login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)

        # step 2: login
        assert self.loginpage.login(self.mainpage, self.username, self.password)

        # step 3: test add
        #assert self.mainpage.add(2, 2)

        # step 3: logout
        assert self.mainpage.logout(self.welcomepage)


    def test_02_test_login_page_login_fail(self):

        assert self.welcomepage.click_login(self.loginpage)

        assert self.loginpage.multi_page_elements_test("test")

        assert self.loginpage.home(self.welcomepage)

    def test_03_test_popup(self):

        assert self.welcomepage.click_login(self.loginpage)

        assert self.loginpage.login(self.mainpage, self.username, self.password)

        assert self.mainpage.say_something("OK")

        assert self.mainpage.logout(self.welcomepage)

    def test_04_test_frames(self):

        assert self.welcomepage.click_login(self.loginpage)

        assert self.loginpage.login(self.mainpage, self.username, self.password)

        assert self.mainpage.goto_frames(self.framespage)

        assert self.framespage.switch_click_on_main_frame()

        assert self.mainpage.logout(self.welcomepage)

    def test_05_test_dropdowns(self):
        assert self.welcomepage.click_login(self.loginpage)

        assert self.loginpage.login(self.mainpage, self.username, self.password)

        assert self.mainpage.add(2, 2)

        assert self.mainpage.logout(self.welcomepage)
